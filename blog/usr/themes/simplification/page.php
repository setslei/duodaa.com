<?php
$this->need('header.php');
?>
<h2> <?php $this->title() ?> </h2>
<div class="post">
<?php $this->content(); ?>
</div>
<?php $this->need('comments.php'); ?>
<?php $this->need('footer.php'); ?>