<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=<?php $this->options->charset(); ?>" />
<title>
<?php $this->options->title(); ?>
<?php $this->archiveTitle(); ?>
</title>
<link rel="stylesheet" type="text/css" media="all" href="<?php $this->options->themeUrl('style.css'); ?>" />
<?php $this->header(); ?>
</head>

<body>
<div id="page">
<div id="head">
<div id="nav">
<ul>
<li><a href="<?php $this->options->siteUrl(); ?>">home</a></li>
<?php $this->widget('Widget_Contents_Page_List')->parse('<li><a href="{permalink}">{title}</a></li>'); ?>
</ul>
</div>
<h1>
<a href="<?php $this->options->siteUrl(); ?>"><?php $this->options->title() ?></a> 
</h1>

</div>