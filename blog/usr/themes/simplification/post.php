<?php $this->need('header.php'); ?>

<h2 class="title"><?php $this->title() ?></h2>
<div class="post">
<?php $this->content(); ?>
<p class="info">
<?php $this->date('F j, Y, g:i A'); ?>　<a href="<?php $this->permalink() ?>">Permalink</a></p>
</div>

<span class="left"><?php $this->theNext("← %s",""); ?></span>
<span class="right"><?php $this->thePrev("%s → ",""); ?></span>


<?php $this->need('comments.php'); ?>
<?php $this->need('footer.php'); ?>
