<?php function threadedComments($comments, $singleCommentOptions) {

$commentClass = '';
if ($comments->authorId) {
if ($comments->authorId == $comments->ownerId) {
$commentClass .= ' comment-by-author';
} else {
$commentClass .= ' comment-by-user';
}
}

?>

<li id="<?php $comments->theId(); ?>" class="parent comment <?php
if ($comments->_levels > 0) {
echo ' comment-child';
} else {
echo ' comment-parent';
}
?>">


<div class="comment-body" id="div-<?php $comments->theId(); ?>">
<div class="comment-author">
<?php $comments->gravatar("40", "40"); ?>
</div>

<cite class="fn"> <?php $comments->author(); ?>　<a href="<?php $comments->permalink(); ?>"><?php $comments->date('Y.n.j H:i'); ?></a></cite>

<span class="reply"><?php $comments->reply($singleCommentOptions->replyWord); ?></span>

<div class="comments-content">
<?php $comments->content();  ?>
</div>
<?php if ($comments->children) { ?>
<ul class="comment-children"><?php $comments->threadedComments($singleCommentOptions); //评论嵌套 ?></ul>

<?php } ?>

</li>

<?php
}
?>

<div id="comment">


<?php $this->comments()->to($comments); ?>
<?php if ($comments->have()): ?>

<h3>所有评论</h3>


<?php $comments->listComments(); ?>

<?php $comments->pageNav(); ?>
<?php endif; ?>


<?php if($this->allow('comment')): ?>






<div id="<?php $this->respondId(); ?>" class="respond">

<h4>发表评论<span class="cancel-comment-reply">　<?php $comments->cancelReply('click here to cancel reply'); ?>
</span></h4>

<form method="post" action="<?php $this->commentUrl() ?>" id="comment_form">

<?php if($this->user->hasLogin()): ?>

<p>Logged in as <a href="<?php $this->options->adminUrl(); ?>"><?php $this->user->screenName(); ?></a>.
<a href="<?php $this->options->logoutUrl(); ?>"><?php _e('Logout'); ?></a>&raquo;</p>

<?php else: ?>

<p><label>名称：</label><input type="text" name="author" class="text" size="35" value="<?php $this->remember('author'); ?>" /></p>
<p><label>邮箱：</label><input type="text" name="mail" class="text" size="35" value="<?php $this->remember('mail'); ?>" /></p>
<p><label>网站：</label><input type="text" name="url" class="text" size="35" value="<?php $this->remember('url'); ?>" /></p>
<?php endif; ?>

<p><textarea rows="10" cols="60" name="text"><?php $this->remember('text'); ?></textarea></p>
<p><input type="submit" value="提交评论" class="submit" /></p>
</form>
<?php else: ?>
<h4><?php _e('禁止评论'); ?></h4>
<?php endif; ?>
</div>
</div>

