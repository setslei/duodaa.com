<?php
/**
* 仿Tumblr上的simplification
*
* @package simplification for typecho
* @author zh369ao
* @version 1.2
* @link http://zh369ao.zzl.org
*/

$this->need('header.php');
?>
<div id="posts">
<?php while($this->next()): ?>
<div class="post">
<h2><?php $this->title() ?></h2>

<?php $this->content('read more'); ?>

<p class="info">
<?php $this->date('F j, Y, g:i A'); ?>　<a href="<?php $this->permalink() ?>">Permalink</a></p>

</div>
<?php endwhile; ?>
</div>
<?php $this->pageLink('←Older Posts', 'next'); ?>
<?php $this->pageLink('Newer Posts→', 'prev'); ?>
<p class="clear"></p>
<?php $this->need('footer.php'); ?>