<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh" lang="zh">
<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="content-type" content="text/html; charset=<?php $this->options->charset(); ?>" />
<title><?php $this->archiveTitle(' &raquo; ', '', ' - '); ?><?php $this->options->title(); if ($this->is('index')) echo ' - ', $this->options->description; $paded = Typecho_Widget::widget('Widget_Archive')->request->page; if ($paded) echo ' - Page ',$paded;?></title>
<link rel="shorcut icon" href="<?php $this->options->themeUrl('images/favicon.ico'); ?>" type="image/x-ico" />
<link rel="stylesheet" type="text/css" media="all" href="<?php $this->options->themeUrl('style.css'); ?>" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.2.6/jquery.min.js"></script>
<script type="text/javascript" src="<?php $this->options->themeUrl('all.js'); ?>"></script>
<?php $this->header(); ?>
</head>
<body>
<div id="header" class="container_16 clearfix">
    <form id="search" method="post" action="">
      <p><input type="submit" class="submit" value="" /><input type="text" name="s" /></p>
    </form>
    <div id="login">
		<ul>
     <?php $this->widget('Widget_Contents_Page_List')->parse('<a href="{permalink}">{title}</a>'); ?>
        </ul>
    </div>
    <div id="logo">
<?php $hh = $this->is('single') ? 'div' : 'h1'; ?>
        <<?php echo $hh; ?> id="site_title"><a href="<?php $this->options->siteUrl(); ?>"><?php if ($this->options->logoUrl): ?><img src="<?php $this->options->logoUrl(); ?>" alt="<?php $this->options->title(); ?>" /><?php endif; ?><?php $this->options->title(); ?></a></<?php echo $hh; ?>>
        <div class="description"><?php $this->options->description() ?></div>
    </div>
</div><!-- end #header -->
<div class="container container_16">
