<?php $this->need('header.php'); ?>

    <div class="grid_10" id="content">
    <?php if ($this->have()): ?>
	<?php while($this->next()): ?>
        <!--开始-->
                    <div class="box text">

                            <a href="<?php $this->permalink() ?>" class="box-tag">
                                <span class="month"><?php $this->date('F'); ?></span><br/>
                                <span class="day"><?php $this->date('j'); ?></span>
                            </a>
                        <div class="title">
                                <a href="<?php $this->permalink() ?>">
								<?php $this->title() ?></a>
                        </div>

                            <div class="caption rich-content"><?php $this->content('让我膜拜全文吧'); ?></div>
                        <div class="box-footer">
                            <ul>
                                <li>
                                    <p class="time">
                                            <span class="mr10 fr"><a href="<?php $this->permalink() ?>">
											全文</a></span>
                                            <span class="mr10"><?php $this->date('Y'); ?></span>

                                        </p>
                                </li>
                            </ul>
                        </div>
                    </div>
<!--/结束-->
	<?php endwhile; ?>
    <?php else: ?>
        <div class="post">
            <h2 class="entry_title"><?php _e('没有找到内容'); ?></h2>
        </div>
    <?php endif; ?>

       <div class="pagination">
		<?php $this->pageLink('&laquo; Previous', 'prev'); ?>
		<?php $this->pageLink('Next &#187;', 'next'); ?>
					<p class="clear"></p>
            </div>
	<?php $this->need('sidebar.php'); ?>
	<?php $this->need('footer.php'); ?>