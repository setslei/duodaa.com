<?php
/**
 * 一个比较清洁的模版，灰色系
 * 
 * @package duodaa_blog
 * @author DuodaaMaster
 * @version 1.0.0.1
 * @link http://www.duodaa.com
 */
 
 $this->need('header.php');
 ?>

        <?php while($this->next()): ?>
<!--开始-->
                    <div class="box text">

                            <a href="<?php $this->permalink() ?>" class="box-tag">
                                <!--<span class="month"><?php $this->date('n月d日'); ?></span>-->
                                <span class="day"><?php $this->date('n月d日'); ?></span>
                            </a>
                        <div class="title">
                                <a href="<?php $this->permalink() ?>">
								<?php $this->title() ?></a>
                        </div>

                            <div class="caption rich-content"><?php $this->excerpt(300,''); ?></div>
                        <div class="box-footer">
                            <ul>
                                <li>
                                    <p class="time">
                                            <span class="mr10 fr">
                                                <a href="<?php $this->permalink() ?>">阅读全文</a>
                                            </span>
                                            <span class="mr10"><?php $this->date('Y年n月d日'); ?></span>

                                        </p>
                                </li>
                            </ul>
                        </div>
                    </div>
<!--/结束-->
<?php endwhile; ?>
            <div class="pagination">
		<?php $this->pageLink('&laquo; Previous', 'prev'); ?>
		<?php $this->pageLink('Next &#187;', 'next'); ?>
					<p class="clear"></p>
            </div>
	<?php $this->need('sidebar.php'); ?>
	<?php $this->need('footer.php'); ?>
