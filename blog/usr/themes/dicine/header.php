﻿<!DOCTYPE html>
<html lang="zh-cn">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title><?php $this->archiveTitle(' &raquo; ', '', ' - '); ?><?php $this->options->title(); ?></title>
    <link rel="stylesheet" type="text/css" media="all" href="<?php $this->options->themeUrl('style.css'); ?>" />
    <script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>

    <script type="text/x-mathjax-config">
          MathJax.Hub.Config({
            extensions: ["tex2jax.js","asciimath2jax.js","TeX/AMSmath.js","Tex/AMSsymbols.js","MathMenu.js","MathZoom.js"],
            jax: ["input/TeX","output/HTML-CSS"],
            tex2jax: {
              inlineMath: [ ['$','$']],
              displayMath: [ ],
              processEscapes: true
            },
            "HTML-CSS": { availableFonts: ["TeX"] }
          });
    </script>


    <script type="text/javascript" src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_HTML"></script>

    <style>
        body{ background-color:#eee;}
        body{ color:#555;}
        a{color:#365C7F}
        a:hover{color:#860000}
        .caption a{color:#860000; text-decoration:none;}
        .caption a:hover{color:#365C7F; text-decoration:underline;}

    </style>
<?php $this->header(); ?>
</head>
<body>
   <!--adpush升窗广告-->
   <script type="text/javascript" src="http://f2.adpush.cn/ap/adspacejs/53249.js"></script>
   <!--adpush升窗广告-->
    <div class="wrapper">
        <div class="header">
            <!--title-->
            <h2><a href="<?php $this->options->siteUrl(); ?>"><?php $this->options->title() ?></a></h2>
        </div>
    <div class="container" id="container">
        <div class="main-content">