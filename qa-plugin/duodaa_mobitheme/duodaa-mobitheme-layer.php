<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 14-1-31
 * Time: 上午11:52
 */

class qa_html_theme_layer extends qa_html_theme_base {

    var $duodaa_mobi_root = '../duodaa_mobitheme/';
    var $css_names =array(

        //'jquery.mobile.theme-1.4.1.min.css',
        'http://code.jquery.com/mobile/1.4.0/jquery.mobile.structure-1.4.0.min.css',
         'qa-styles.css',
        );

    var $script_names = array(
        //'iScroll.js',
       // 'Jquery1.11.js',
        'http://code.jquery.com/jquery-1.9.1.min.js',
        'http://code.jquery.com/mobile/1.4.1/jquery.mobile-1.4.1.min.js'
    );

    var $math_jax_config = 'MathJax_config.js';
    var $math_jax_src = 'http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS_HTML';


    function head_css()
    {

             $src = $this->rooturl.$this->duodaa_mobi_root.$this->css_name;

             foreach($this->css_names as $css_name)
             {
                 if(substr($css_name,0,5)=='http:')
                 {
                     $this->output_raw('<LINK REL="stylesheet" TYPE="text/css" HREF="'.$css_name.'"/>');
                 }
                 else
                 {
                     $this->output_raw('<LINK REL="stylesheet" TYPE="text/css" HREF="'.$this->rooturl.$this->duodaa_mobi_root.$css_name.'"/>');
                 }
             }

    }

    function head_lines()
    {

    }


    function head_script()
    {

       // qa_html_theme_base::head_script();


        foreach ($this->script_names as $script_name)
        {
            if(substr($script_name,0,5)=='http:')
            {
                $this->output_raw('<script src="'.$script_name.'" type="text/javascript"></script>');
            }
            else
            {
            $this->output_raw('<script src="'.$this->rooturl.$this->duodaa_mobi_root.$script_name.'" type="text/javascript"></script>');
            }
        }

        $math_jax_cfg = file_get_contents($this->rooturl.$this->duodaa_mobi_root.$this->math_jax_config);
        $this->output_raw('<script type="text/x-mathjax-config">'.$math_jax_cfg.'</script>');
        $this->output_raw('<script src="'.$this->math_jax_src.'" type="text/javascript"></script>');


    }


    function sidepanel()
    {

    }


    function footer()
    {

    }

    function bottom_script()
    {
        $this->output_raw('<script src="'.$this->rooturl.$this->duodaa_mobi_root.'duodaa_mobitheme_js.js'.'" type="text/javascript"></script>');
    }

    function body_footer()
    {
         // if (isset($this->content['body_footer']))
         // $this->output_raw($this->content['body_footer']);
    }

    function header()
    {

        $this->output_raw('<div data-role="header" data-position="fixed" data-tap-toggle="false" style="top:-1px;padding-top:0px">');
        $this->output('<DIV CLASS="qa-header">');


        $this->output_raw('<div style="background-color:#000000;width:100%;height:30px;padding: 0px 0px 0px 0px">');
        $this->output_raw($this->nav('user'));
        $this->output_raw('</div>');

        $this->nav_main_sub();
        $this->header_clear();



        $this->output('</DIV> <!-- END qa-header -->', '');
        $this->output_raw('</div>');
    }

    function nav($navtype, $level=null)
    {
        $navigation=@$this->content['navigation'][$navtype];

        //以下去掉自定义的导航连接
        foreach ($navigation as $key => $navlink)
        {
            if(strpos($key,'custom-')===0)
            {
                unset($navigation[$key]);
            }
        }

        if (($navtype=='user') || isset($navigation)) {
            $this->output('<DIV CLASS="qa-nav-'.$navtype.'">');

            if ($navtype=='user')
                $this->logged_in();

            // reverse order of 'opposite' items since they float right
            foreach (array_reverse($navigation, true) as $key => $navlink)
                if (@$navlink['opposite']) {
                    unset($navigation[$key]);
                    $navigation[$key]=$navlink;
                }

            $this->set_context('nav_type', $navtype);
            $this->nav_list($navigation, 'nav-'.$navtype, $level);
            $this->nav_clear($navtype);
            $this->clear_context('nav_type');

            $this->output('</DIV>');
        }
    }

    function voting($post)
    {

            if (isset($post['vote_view'])) {
                $this->output('<DIV CLASS="qa-voting '.(($post['vote_view']=='updown') ? 'qa-voting-updown' : 'qa-voting-net').'" '.@$post['vote_tags'].'>');
                $this->voting_inner_html($post);
                $this->output('</DIV>');
            }

    }

    function voting_inner_html($post)
    {
        if($this->template == 'question')
        {
           $this->vote_buttons($post);
        }
        $this->vote_count($post);
        $this->vote_clear();
    }

    function nav_main_sub()
    {
        $this->nav('main');
        //$this->nav('sub');
    }

    function body()
    {
        $this->output('<BODY ');
        $this->body_tags();
        $this->output('>');

        $this->body_script();
        $this->body_header();
        $this->body_content();
        $this->body_footer();
        $this->body_hidden();

        $this->bottom_script();

        $this->output('</BODY>');
    }

    function body_content()
    {
        $this->body_prefix();
        $this->notices();

        $this->header();

        $this->output_raw('<div data-role="content" style="padding: 60px 0px 0px 0px;">');

        $this->main();

        $this->output_raw('</div>');

         $this->footer();

         $this->body_suffix();
    }

    function main()
    {
        $content=$this->content;

        $this->main_parts($content);

        $this->page_links();
        $this->suggest_next();


    }

    function q_view($q_view)
    {
        if (!empty($q_view)) {

            //var_dump($q_view);
            $this->output('<div class="qa-q-view'.(@$q_view['hidden'] ? ' qa-q-view-hidden' : '').rtrim(' '.@$q_view['classes']).'"'.rtrim(' '.@$q_view['tags']).'>');

            if (isset($q_view['main_form_tags']))
                $this->output('<form '.$q_view['main_form_tags'].'>'); // form for voting buttons

            $this->q_view_stats($q_view);

            if (isset($q_view['main_form_tags'])) {
                $this->form_hidden_elements(@$q_view['voting_form_hidden']);
                $this->output('</form>');
            }

            //$this->output_raw($this->content["title"]);
            $this->q_view_main($q_view);
            $this->q_view_clear();

            $this->output('</div> <!-- END qa-q-view -->', '');
        }
    }


    function q_view_main($q_view)
    {

        $this->output_raw($q_view['title']);

        $this->output('<div class="qa-q-view-main">');

        if (isset($q_view['main_form_tags']))
            $this->output('<form '.$q_view['main_form_tags'].'>'); // form for buttons on question


        $this->view_count($q_view);
        $this->q_view_content($q_view);
        $this->q_view_extra($q_view);
        $this->q_view_follows($q_view);
        $this->q_view_closed($q_view);
        $this->post_tags($q_view, 'qa-q-view');
        $this->post_avatar_meta($q_view, 'qa-q-view');
        $this->q_view_buttons($q_view);
        $this->c_list(@$q_view['c_list'], 'qa-q-view');

        if (isset($q_view['main_form_tags'])) {
            $this->form_hidden_elements(@$q_view['buttons_form_hidden']);
            $this->output('</form>');
        }

        $this->c_form(@$q_view['c_form']);

        $this->output('</div> <!-- END qa-q-view-main -->');
    }


    function q_list_items($q_items)
    {
        foreach ($q_items as $q_item)
            $this->q_list_item($q_item);
    }

    function q_list_item($q_item)
    {
        $this->output('<div class="qa-q-list-item'.rtrim(' '.@$q_item['classes']).'" '.@$q_item['tags'].'>');

        //var_dump($q_item);
        $this->q_item_stats($q_item);
        $this->q_item_main($q_item);
        $this->q_item_clear();

        $this->output('</div> <!-- END qa-q-list-item -->', '');
    }


}
