<?php

class qa_html_theme_layer extends qa_html_theme_base
{
    function doctype()
    {

    }

    function html()
    {


        $content = $this->content;
        //$data='{}';

        switch($this->template)
        {
            case 'qa':                                                            //qa表示首页的问题列表
                $qs=$content['q_list']['qs'];                                    //装入q_list的qs信息
                foreach($qs as $key=>$q)
                {
                    $data[$key]['postid']=$q['raw']['postid'];                     //问题id
                    $data[$key]['title']=$q['raw']['title'];                  //问题名称
                    $data[$key]['userid']=$q['raw']['userid'];                //提问人id
                    $data[$key]['handle']=$q['raw']['handle'];                //提问者用户名
                    $data[$key]['created']=date('Y-m-d H:i:s',$q['raw']['created']);             //提问时间
                    $data[$key]['url']=$q['url'];                              //问题链接
                    $data[$key]['answers_raw']=$q['answers_raw'];           //回答数
                }
                break;

            case 'questions':                                                    //qa表示一般的问题列表,暂时和qa的情况一样
                $qs=$content['q_list']['qs'];                                    //装入q_list的qs信息
                foreach($qs as $key=>$q)
                {
                    $data[$key]['postid']=$q['raw']['postid'];                     //问题id
                    $data[$key]['title']=$q['raw']['title'];                  //问题名称
                    $data[$key]['userid']=$q['raw']['userid'];                //提问人id
                    $data[$key]['handle']=$q['raw']['handle'];                //提问者用户名
                    $data[$key]['tags']=$q['raw']['tags'];                    //标签，用逗号分隔
                    $data[$key]['created']=date('Y-m-d H:i:s',$q['raw']['created']);             //提问时间
                    $data[$key]['url']=$q['url'];                              //问题链接
                    $data[$key]['answers_raw']=$q['answers_raw'];           //回答数
                }
                break;

            case 'question':                                                //question表示问题的具体页

                if(!isset($content['error']))
                {
                $q_view=$content['q_view'];

                $data['q_view']['postid']=$q_view['raw']['postid'];                     //问题id
                $data['q_view']['title']=$q_view['raw']['title'];                  //问题名称
                $data['q_view']['content']=$q_view['raw']['content'];             //问题内容
                $data['q_view']['userid']=$q_view['raw']['userid'];                //提问人id
                $data['q_view']['handle']=$q_view['raw']['handle'];                //提问者用户名
                $data['q_view']['tags']=$q_view['raw']['tags'];                    //标签，用逗号分隔
                $data['q_view']['created']=date('Y-m-d H:i:s',$q_view['raw']['created']);             //提问时间
                $data['q_view']['url']=$q_view['url'];                              //问题链接

                //以下搞定问题的评论列表
                $cs= $q_view['c_list']['cs'];
                foreach($cs as $key=>$c)
                {
                    $data['q_view']['c_list'][$key]['postid']=$c['raw']['postid'];
                    $data['q_view']['c_list'][$key]['content']=$c['raw']['content'];
                    $data['q_view']['c_list'][$key]['userid']=$c['raw']['userid'];
                    $data['q_view']['c_list'][$key]['handle']=$c['raw']['handle'];
                    $data['q_view']['c_list'][$key]['created']=date('Y-m-d H:i:s',$c['raw']['created']);

                }

                 //以下搞定问题的的回答列表
                $as=$content['a_list']['as'];                           //
                foreach($as as $key=>$a)
                {
                    $data['a_list'][$key]['postid']=$a['raw']['postid'];
                    $data['a_list'][$key]['content']=$a['raw']['content'];
                    $data['a_list'][$key]['userid']=$a['raw']['userid'];
                    $data['a_list'][$key]['handle']=$a['raw']['handle'];
                    $data['a_list'][$key]['created']=date('Y-m-d H:i:s',$a['raw']['created']);
                    $data['a_list'][$key]['isselected']=$a['raw']['isselected'];

                    //以下对每一个回答，搞定评论列表
                    $cs= $a['c_list']['cs'];
                    foreach($cs as $k=>$c)
                    {
                        $data['a_list'][$key]['c_list'][$k]['postid']=$c['raw']['postid'];
                        $data['a_list'][$key]['c_list'][$k]['content']=$c['raw']['content'];
                        $data['a_list'][$key]['c_list'][$k]['userid']=$c['raw']['userid'];
                        $data['a_list'][$key]['c_list'][$k]['handle']=$c['raw']['handle'];
                        $data['a_list'][$key]['c_list'][$k]['created']=date('Y-m-d H:i:s',$c['raw']['created']);

                    }
                }


                }

                if(isset($content['error']))$data['error']=$content['error'];


                break;

        }

        if(strpos($this->template,'user') === 0 )
         {
            require_once QA_PLUGIN_DIR."/duodaa_data_for_client/data_user.php";
            $handle = isset($_POST["handle"])?$_POST["handle"]:'0';
            $password =isset($_POST["password"])?$_POST["password"]:'0';

            $user=duodaa_login($handle,$password);

             if($user['error']!="") $data['error']=$user['error'];

             if(!$user['error']==""){
                    switch($this->template)
                    {
                        case "user":

                                $data['aposts'] =$content['raw']['points']['aposts'];
                                $data['selected']=$content['raw']['points']['aselecteds'];
                                $data['selected_rate']= floor($data['selected']/$data['aposts']*100).'%';

                        break;

                        case "user-questions":
                               $qs=$content['q_list']['qs'];
                               foreach($qs as $key=>$q)
                                {
                                    $data[$key]['postid']=$q['raw']['postid'];                     //问题id
                                    $data[$key]['title']=$q['raw']['title'];                  //问题名称
                                    $data[$key]['userid']=$q['raw']['userid'];                //提问人id
                                    $data[$key]['handle']=$q['raw']['handle'];                //提问者用户名
                                    $data[$key]['created']=date('Y-m-d H:i:s',$q['raw']['created']);             //提问时间
                                    $data[$key]['url']=$q['url'];                              //问题链接
                                    $data[$key]['answers_raw']=$q['answers_raw'];           //回答数
                                }
                               //var_dump($qs); exit;
                        break;

                        default:
                            echo $this->template;
                            exit;
                        break;

                    }
             }

        }

        //echo $this->template; exit;
        //$data['content']=$content['q_list'];



        //var_dump($data);
        header("Access-Control-Allow-Origin: *");
        header("Content-type:text/html;charset=utf-8");
        if(!isset($data))
        {
            $data='{}';
        }
        echo $this->to_JSON($data);

        //echo $this->template;
        //var_dump($data);
        //var_dump($qs);
        //var_dump($content['a_list']['as']);
        //var_dump($content['q_view']);
        //var_dump($content['raw']['points']);
    }


    //下面的函数把用户id变成用户名
    function userid_to_handle($uid)
    {
        require_once QA_INCLUDE_DIR.'qa-app-users.php';
        $handles = qa_userids_to_handles(array($uid));   //提问者用户名的数组，数组应该只有一个数据
        return $handles[$uid];                             //返回提问者用户名
    }

    //下面的函数把数组变成Json数据格式
    function to_JSON($array_souce)
    {
        //奖数组编码成Json
        $json = json_encode($array_souce);

        //以下处理中文问题
        $json = preg_replace("#\\\u([0-9a-f]{4})#ie", "iconv('UCS-2BE', 'UTF-8', pack('H4', '\\1'))", $json);

        //以下处理小于号"<"的问题
        $json=str_replace('<','&lt;',$json);

        return $json;
    }

}