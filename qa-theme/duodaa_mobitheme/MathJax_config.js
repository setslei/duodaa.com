MathJax.Hub.Config({
    extensions: ["tex2jax.js","asciimath2jax.js","TeX/AMSmath.js","Tex/AMSsymbols.js","MathMenu.js","MathZoom.js"],
    jax: ["input/TeX","output/HTML-CSS"],
    tex2jax: {
        inlineMath: [ ['$','$']],
        displayMath: [ ],
        processEscapes: true
    },
    "HTML-CSS": { availableFonts: ["TeX"] }
});