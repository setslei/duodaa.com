﻿<?php

/*
	Question2Answer (c) Gideon Greenspan

	http://www.question2answer.org/

	
	File: qa-theme/Candy/qa-theme.php
	Version: See define()s at top of qa-include/qa-base.php
	Description: Override something in base theme class for Candy theme


	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.
	
	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	More about this license: http://www.question2answer.org/license.php
*/

	class qa_html_theme extends qa_html_theme_base
	{	
		// adding ie specific css
		function head_script()
		{
			$this->output('<meta name="keywords" content="数学,应用数学,数学辅导,数学题,数学解答,数学考试,数学帮助,基础数学,LaTex,公式,在线数学,数学互助,数学教育" />');
			if (isset($this->content['script']))
				foreach ($this->content['script'] as $scriptline)
					$this->output_raw($scriptline);
			
			$this->output('<!--[if IE]>');	
			$this->output('<LINK REL="stylesheet" TYPE="text/css" HREF="'.$this->rooturl.$this->ie_css().'"/>');
			$this->output('<![endif]-->');



			
		}
		
				
		function ie_css()
		{
			return 'ie.css';		
		}

        function header()
		
		{
			$this->output('<DIV CLASS="qa-header">');
			
			$fheader = fopen($this->rooturl.'headerlinks.ini','r');
			$this->output('<div style="text-align: right; width:97%;height:20px;padding-top: 5px;padding-bottom: 1px; ">',"\n");
			
			while (!feof($fheader))
			{
			 $headerinfos=explode(',',fgets($fheader));
			 if(isset($headerinfos[2])&& $headerinfos[2]==1)
			 {
			   $headerlinks=trim($headerinfos[1]);
			   $headertext=trim($headerinfos[0]);
			   if(substr($headerlinks,0,5)!='http:')$headerlinks=$this->rooturl.$headerlinks;
			   
			   $this->output('<a href="'.$headerinfos[1].'">'.$headerinfos[0].'</a>',"\n");
			 }
			
			}
		
			$this->output('</div>',"\n");
			fclose($fheader);
			
			$this->logo();
			$this->nav_user_search();
			$this->nav_main_sub();
			$this->header_clear();
			
			
			
			$this->output('</DIV> <!-- END qa-header -->', '');
			
			
			}
	
		// header part
		function nav_user_search() // reverse the usual order
		{
			$this->search();
			$this->nav('user');
			
			
		}
		
		// main content div
		function q_view_content($q_view)
		{
			if (!empty($q_view['content']))
				$this->output(
					'<DIV CLASS="qa-q-view-content clearfix">',
					$q_view['content'],
					'</DIV>'
				);
		}
		
		// custom footer
		function footer()
        {
            $this->output('<DIV CLASS="qa-footer">');			
			
			$this->output('<DIV CLASS="footer-copyright">');
			$this->output('<p>Copyright &copy; '.date('Y').' '.$this->content['site_title'].' - 版权所有.</p>');
			$this->output('</DIV>');
			
			$this->attribution();							
			
			//$this->output('<DIV CLASS="footer-credit">');
			//$this->output('<p>Theme Designed By: <a href="http://pixelngrain.com">Pixel n Grain</a></p>');
			//$this->output('</DIV>');
			
			$this->nav('footer');	
			
			$this->footer_clear();
            
            $this->output('</DIV>');
            
           
            $this->output('<DIV style="border:1px solid #eee;margin:5px;padding:3px"><div style="padding:8px 0px 8px 0px" >友情链接</div>');
            $this->output('<DIV>');
            
            $path=$_SERVER['DOCUMENT_ROOT'].'\friendlink.ini';
			$file = fopen( $path,'r');
			$html='';
			while(!feof($file))
			{
			$linksource = explode(',',fgets($file));
		
			$linktext = trim($linksource[0]);
			
			if(isset($linksource[1]))
			{$link=trim($linksource[1]);}
			else
			{$linktext = '#';
			$link='#';}
			
			$html .= '<a href="'.$link.'" target="_blank">'.$linktext.'</a>';
			
			if( isset($linksource[2]))
			{$html .= '<br>'."\n";
			}
			else 
			{$html .= "\n";
			}
			}
			fclose($file);
			
			$this->output($html);
			$this->output('</DIV>');
            
            $this->output('</DIV>');
            $this->output(' <!-- END qa-footer -->');

            $this->output('<script>$(".qa-part-q-view").before("<div class=\'bdsharebuttonbox\' data-tag=\'share_1\' style=\'margin:10px 0 10px 25px;\'><a class=\'bds_mshare\' data-cmd=\'mshare\'></a><a class=\'bds_qzone\' data-cmd=\'qzone\' href=\'#\'></a><a class=\'bds_tsina\' data-cmd=\'tsina\'></a><a class=\'bds_baidu\' data-cmd=\'baidu\'></a><a class=\'bds_renren\' data-cmd=\'renren\'></a><a class=\'bds_tqq\' data-cmd=\'tqq\'></a><a class=\'bds_more\' data-cmd=\'more\'>更多</a><a class=\'bds_count\' data-cmd=\'count\'></a></div>");</script>');
         ?>
            <script>

                window._bd_share_config = {
                    common : {
                        bdText : $(".entry-title").html(),
                        //bdDesc : $(".entry-content").html(),
                        bdUrl : window.location.href
                        //bdPic : '自定义分享图片'
                    },
                    share : [{
                        "bdSize" : 24
                    }],
                    slide : [{
                        bdImg : 0,
                        bdPos : "right",
                        bdTop : 100
                    }],
                    image : [{
                        viewType : 'list',
                        viewPos : 'top',
                        viewColor : 'black',
                        viewSize : '16',
                        viewList : ['qzone','tsina','huaban','tqq','renren']
                    }],
                    selectShare : [{
                        "bdselectMiniList" : ['qzone','tqq','kaixin001','bdxc','tqf']
                    }]
                }
                with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?cdnversion='+~(-new Date()/36e5)];
            </script>

            <?php
            $this->output('<script type="text/javascript" src="\qa-content\qa-duodaa.js"></script>');
			

        }
        
        

		function q_list_items($q_items)
		{
			$i=0;

            $ad_script_1 = file_get_contents($this->rooturl.'ads/sogou_item_bottom.ini');
                     

			foreach ($q_items as $q_item)
			{
				$i=$i+1;
				$this->q_list_item($q_item);
				
				if($i==1)  //在第一个的末尾加入广告
				{
					$this->output('<div  style="with:760px;background: #f4f4f4;">'.$ad_script_1.'</div>');
				}
				
				else if($i==3 || $i==6 || $i==19) //在第3、6、19的末尾加入广告
				{
			    	$this->output('<div style="with:760px;background: #f4f4f4;">'.$ad_script_1.'</div>');
				}

			}
		}



		
        
	}
	
	
	 

/*
	Omit PHP closing tag to help avoid accidental output
*/